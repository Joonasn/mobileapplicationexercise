MobileAppExercise
----------------------------------------------------------------------------------------------------------

Contains all the shared functionality of the application.

**App.cs**

Main file of the application.

**MainPage.xaml.cs**

Main page, which is created once the application starts. It includes a search bar.

**Page2.xaml.cs**

The page that gets loaded when user selects GitHub user. Shows repositories from previously selected user.

**Page3.xaml.cs**

The page that gets loaded when user selects repository. Displays maximum of 10 latest commits.

**RestfulClient.cs**

Handles the HTTP API calls.

**Result.cs**

Contains classes for JSON data used in the application.


MobileAppExercise.Android
----------------------------------------------------------------------------------------------------------

Technical specifications for the Android version. Compile this to create an Android apk file. Keystore must be created seperately.


MobileAppExercise.iOS
------------------------------------------------------------------------------------------------------

Technical specifications for the iOS version.


How JSON data consuming works:
------------------------------------------------------------------------------------------------------

When user searches for a Github user or selects a repository, the **current page** makes a call in **API.cs**, which then sends the call to **RestfulClient.cs** for handling.

Once the call is finished, the response is returned to the page and **JSON deserialization** begins. 
When deserializing data, class structures from **Result.cs** are used to handle the data and prevent null JSON information passing through. 
Then the data is sent to **”ObservableCollection”** (a list that Xamarin Forms uses), which is used to display data on user interface.


Technologies:
----------------------------------------------------------------------------------------------------

C#/.NET /Visual Studio

System.NET ( HTTP -calls)

Newton Json (JSON deserialization)

Xamarin Forms (Compiling and user interface)