﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileAppExercise
{
    public partial class Page2 : ContentPage
    {
        public ObservableCollection<Repository> Repositories { get; private set; }
        public Page2(string reposityData)  // Page 2 is created when user presses GitHub profile. When created, data from "MainPage" (repositoryData) gets imported
        {
            InitializeComponent();
            Repositories = new ObservableCollection<Repository>();
            Repositories.Clear();                    
            DeserializeArrayData(reposityData);    // Deserialize imported data
            BindingContext = this;
        }

        private void DeserializeArrayData(string _JSON)
        {
            var DeSerializedArrayData = JsonConvert.DeserializeObject<List<Repository>>(_JSON);
            for (int i = 0; i < DeSerializedArrayData.Count; i++)
            {
                Repositories.Add(new Repository
                {
                    Name = (DeSerializedArrayData[i].Name ?? " "),
                    Id = (DeSerializedArrayData[i].Id),
                    Commits_url = (DeSerializedArrayData[i].Commits_url ?? " "),
                });
            }
        }

        private void Repository_Tapped(object sender, ItemTappedEventArgs e) // When user selects a repository, GET commit information 
        {
            var viewModel = (Repository)e.Item; // Get correct commit URL when tapped
            var url = viewModel.Commits_url;  
            string point = url;     
            string point1 = point.Remove(point.Length - 6);         // Remove the "{/sha}" text at the end
            var Response = ((App)App.Current).CallRestAPI(point1); 

            if (Response != null)
            {
                Navigation.PushAsync(new Page3(Response));  
            }
            else
            {
                DisplayAlert("Alert", "Error while fetching data", "OK");
            }
        }
    }
}