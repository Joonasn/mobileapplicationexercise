﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Newtonsoft.Json;

namespace MobileAppExercise
{
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public ObservableCollection<GitUser> User { get; private set; }  // Xamarin's "Observablecollection" for collected data
        readonly string apiAddress = "https://api.github.com/users/";     // Default API address

        public MainPage()
        {
            InitializeComponent();
            User = new ObservableCollection<GitUser>();
            BindingContext = this;    // Allows to bind user data to user interface (MainPage.xaml)
        }

        private void StartSearch_ButtonPressed(object sender, EventArgs e)
        {
            SearchBar searchBar = (SearchBar)sender; 
            var Keyword = searchBar.Text;         // Get text data from searchbar... 
            var point = apiAddress + Keyword;     // and add it to the API address
            var Response = ((App)App.Current).CallRestAPI(point); // Call RestAPI method in main class (App) with a API address(point)

            if (Response != null)  // if there's a response from API, deserialize data
            {
                User.Clear();
                DeserializeData(Response);
            }
        }

        private void DeserializeData (string _JSON)   // GET data from JSON object
        {
            var DeSerializedData = JsonConvert.DeserializeObject<GitUser>(_JSON);  // JSON object gets deserialized and temporally made into a "GitUser" object
            User.Add(new GitUser    
            {
                Name = (DeSerializedData.Name ?? " "),   // Create a new "GithubUser" for "User"-observablecollection list with values from Json object
                Bio = (DeSerializedData.Bio ?? " "),
                Avatar_url = (DeSerializedData.Avatar_url ?? " "),
                Email = (DeSerializedData.Email ?? " "),
                Blog = (DeSerializedData.Blog ?? " "), // Null-coalescing operator for quick check.
            });
        }

        // Deserialized user data will be shown on UI
        private void User_Tapped(object sender, ItemTappedEventArgs e) // When user presses Github user profile, GET it's repositories
        {
            var viewModel = (GitUser)e.Item;
            var name = viewModel.Name;

            var point = apiAddress + name + "/repos";     // Change API address for correct data
            var Response = ((App)App.Current).CallRestAPI(point);

            if (Response != null)
            {
                Navigation.PushAsync(new Page2(Response));   // If there's a response from API, change page on the application (Page2)
            }
            else
            {
                DisplayAlert("Alert", "Error while fetching data", "OK"); // if null, display error message
            }
        }
    }
}