﻿using System;
using System.Net; // Used for API call
using System.IO;  // StreamReader 
using System.Text;
using System.Collections.Generic;
using System.Net.Http;

namespace MobileAppExercise
{
    public enum HttpVerb  // Restful API methods (only GET is used for now)
    {
        GET,
        POST,
        PUT,
        PATCH,
        DELETE
    }

    class RESTfulClient
    {
        public string Point {get;  set; } // API address

        public string Agent { get;  set; } //User Agent variable for Github API

        public HttpVerb HttpMethod { get; set; } //Get Rest HTTP method 

        public RESTfulClient(string dataPoint, string agent) // Constructor 
        {
            Point = dataPoint;
            Agent = agent;
            HttpMethod = HttpVerb.GET;
        }
        public string GetRequest() // Create a request and acquire response 
        {
            string ApiResponseValue = string.Empty;  // Create a string for API response
            HttpWebRequest Request = (HttpWebRequest)WebRequest.Create(Point); //
            Request.Method = HttpMethod.ToString();
            Request.UserAgent = Agent;

            try 
            {
                using (HttpWebResponse Response = (HttpWebResponse)Request.GetResponse()) // if request was successful, create response
                {
                    if (Response.StatusCode != HttpStatusCode.OK) // if HttpStatusCode is not 200(OK), return null
                    {
                        ApiResponseValue = null;
                    }
                    else
                    {
                        using (Stream ResponseStream = Response.GetResponseStream()) 
                        {
                            if (ResponseStream != null)
                            {
                                using (StreamReader reader = new StreamReader(ResponseStream))
                                {
                                    ApiResponseValue = reader.ReadToEnd(); // Read response with StreamReader object and return it to string
                                }
                            }
                        }
                    } 
                }
            }
            catch // Return null if there's error with request
            {
                ApiResponseValue = null;
            }
            return ApiResponseValue; // Return response from API
        }
    }
}