﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileAppExercise
{
    [XamlCompilation(XamlCompilationOptions.Compile)]  //Enables XAML compiling for all pages of the application
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            MainPage = new NavigationPage(new MainPage());  // Create a main page. As a "NavigationPage" object it handles the page changing/stacking
        }
        readonly string UserAgent = "JNMobileApp"; // Required for Github API
 
        public string CallRestAPI(string point)   // Method for Rest API calls
        {
            RESTfulClient RestClient = new RESTfulClient(point, UserAgent); // Tell RestClient where to get data(string point). UserAgent is a requirement for GitHub API                
            string APIResponse = RestClient.GetRequest();   // Get Request and return response to string variable
            return APIResponse;                             // Return response, so it can be used for deserializing
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
