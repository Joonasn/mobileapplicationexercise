﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace MobileAppExercise
{
    // GET user's name, bio, avatar, email and website values
    public class GitUser : INotifyPropertyChanged // used, if values gets updated 
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { }; // method to update value 

        private string UserName;
        public string Name
        {
            get { return UserName ?? ""; }
            set
            {
                UserName = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Name")); // Update value 
            }
        }

        private string Description;
        public string Bio
        {
            get { return Description ?? ""; }
            set
            {
                Description = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Bio"));
            }
        }

        private string Avatar;
        public string Avatar_url
        {
            get { return Avatar ?? ""; }
            set
            {
                Avatar = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Avatar_url"));
            }
        }

        private string UserEmail;
        public string Email
        {
            get { return UserEmail ?? ""; }
            set
            {
                UserEmail = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Email"));
            }
        }

        private string Website;
        public string Blog
        {
            get
            { return Website ?? ""; }
            set
            {
                Website = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Blog"));
            }
        }

        public GitUser() // Constructor 
        {

        }
    }

    public class Repository : INotifyPropertyChanged // GET repository's name and id
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        private string RepoName;
        public string Name
        {
            get { return RepoName ?? " "; }
            set
            {
                RepoName = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Name"));
            }
        }

        private int RepoID; 
        public int Id
        {
            get{ return RepoID; } // If int value is null/empty, it's value is 0.
            set
            {
                RepoID = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Id"));
            }
        }

        private string CommitURL;
        public string Commits_url
        {
            get
            { return CommitURL ?? ""; }
            set
            {
                CommitURL = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Commits_url"));
            }
        }
        public Repository() // Constructor 
        {

        }
    }

    //GET following data about commit: name, date, message and avatar_url. (Login name also can be useful)
    public class Commit
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        private string _Name; // Variables to present commit data in UI 
        public string Name
        {
            get { return _Name ?? ""; }
            set 
            { 
                _Name = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Name"));
            }
        }

        private DateTime _Date;
        public DateTime Date
        {
            get { return _Date; }
            set
            {
                _Date = value;
                if (_Date == null)
                {
                    _Date = DateTime.Now;
                }
                PropertyChanged(this, new PropertyChangedEventArgs("Date"));
            }
        }

        private string _Message;
        public string Message
        {
            get { return _Message ?? ""; }
            set 
            { 
                _Message = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Message"));
            }
        }

        private string _Login;
        public string Login
        {
            get { return _Login ?? ""; }
            set 
            { 
                _Login = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Login"));
            }
        }

        private string Avatar;
        public string Avatar_url
        {
            get { return Avatar ?? ""; }
            set 
            { 
                Avatar = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Avatar_url"));
            }
        }

        public Commit() // Constructor 
        {

        }

        // To consume the commit data, various child nodes need to be arranged correctly. For that theres seperate nested class
        public class CommitRootObject  // Commit RootObject includes a lot commit data, of which I included only the most essential here
        {
            private Commit Committer;
            public Commit commit // Class for "Commit" child node
            {
                get { return Committer ?? (Committer = new Commit()); } // Create new class object if null
            }

            private Author _Authori;
            public Author author  //  Class for "Author" child node
            {
                get { return _Authori ?? (_Authori = new Author()); }
            }

            public class Commit
            {
                private Author Authori;
                public Author author // A child node of "Commit" -node. It's nested so it doesn't mix up with node(s) with the same name
                {
                    get { return Authori ?? (Authori = new Author()); }
                }

                private string _Message;
                public string Message
                {
                    get { return _Message ?? ""; }
                    set { _Message = value; }
                }

                public class Author
                {
                    private string _Name;
                    public string Name
                    {
                        get { return _Name ?? ""; }
                        set { _Name = value; }
                    }

                    private string _Email;
                    public string Email
                    {
                        get { return _Email ?? ""; }
                        set { _Email = value; }
                    }

                    private DateTime _Date;
                    public DateTime Date
                    {
                        get { return _Date; }
                        set
                        {
                            _Date = value;
                            if (_Date == null)
                            {
                                _Date = DateTime.Now;
                            }
                        }
                    }
                }
            }

            public class Author // Child node including avatar url and commiters username
            {
                private string _Login;
                public string Login
                {
                    get { return _Login ?? ""; }
                    set { _Login = value; }
                }
                private string _Avatar;
                public string Avatar_url
                {
                    get { return _Avatar ?? ""; }
                    set { _Avatar = value; }
                }
            }

            public CommitRootObject() // Constructor 
            {

            }
        }
    }
}