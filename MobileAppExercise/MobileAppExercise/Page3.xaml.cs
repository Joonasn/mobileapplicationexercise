﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileAppExercise
{
    public partial class Page3 : ContentPage
    {
        public ObservableCollection<Commit> Commits { get; private set; }
        public Page3(string Commit)
        {
            InitializeComponent();
            Commits = new ObservableCollection<Commit>();
            Commits.Clear();
            DeserializeCommit(Commit);
            BindingContext = this;
        }

        private void DeserializeCommit(string _JSON)   // Load 10 lastest commits to observableCollection
        {
            var DeSerializedCommitData = JsonConvert.DeserializeObject<List<Commit.CommitRootObject>>(_JSON);
            int MaxCount = 10;
            if (DeSerializedCommitData.Count < 10)   // Check if repository has less than 10 commits
            {
                MaxCount = DeSerializedCommitData.Count;  
            }

            for (int i = 0; i < MaxCount; i++)
            {
                Commits.Add(new Commit
                {
                    Name = (DeSerializedCommitData[i].commit.author.Name ?? " "),  // Commit data is seperated in to various child nodes...
                    Date =  (DeSerializedCommitData[i].commit.author.Date),  // Consuming the correct data requires more detailed class structure (Commit-class)
                    Message = (DeSerializedCommitData[i].commit.Message ?? " "),   
                    Login = (DeSerializedCommitData[i].author.Login ?? " "),
                    Avatar_url = (DeSerializedCommitData[i].author.Avatar_url ?? " "),
                });
            }
        }
    }
}